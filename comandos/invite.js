function Invite(para, canal, from, clients) {

	if(para && canal){
		var msg = ' ';	
		clients.forEach(function(client) {
			if(client.nick === para) {
				client.irc_client.emit('invite', canal, from.nick, msg);
			}
		});
	}
}

module.exports = Invite;
