function Privmsg(param, cl)
{

    if(param[1])
    {
        var msg = "";

        if(param[2])
        {    
            for(var i = 2; i < param.length ; i++)
            {
                 msg += param[i] + " ";
            }
        } 


        cl.irc_client.say(param[1],'(Privado) '+cl.nick+': '+ msg);

        cl.emit('selfMessage',param[1], msg);     
    } 

}

module.exports = Privmsg;
