var express = require('express');  
var app = express();		   
var server = require('http').Server(app);
var io = require('socket.io')(server);
var bodyParser = require('body-parser');  
var cookieParser = require('cookie-parser');  
var irc = require('irc');
var socketio_cookieParser = require('socket.io-cookie'); 
var path = require('path');	



var Nick = require('./comandos/nick');
var Privmsg = require('./comandos/privmsg');
var List = require('./comandos/list');
var Ping = require('./comandos/ping');
var Join = require('./comandos/join');
var PrivmsgChannel = require('./comandos/privmsg-channel');

var Invite = require('./comandos/invite');
var Whois = require('./comandos/whois');

io.use(socketio_cookieParser); 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));


var proxy_id = 0;
var clients = [];


app.get('/', function (req, res) {
	

	if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) 
	{		
		proxy_id++;


		res.cookie('id', proxy_id);
		res.sendFile(path.join(__dirname, '/index.html'));		

	}
	else 
	{
		res.sendFile(path.join(__dirname, '/login.html'));
	}
});


io.on('connection', function (socket) {
	
	var client = socket;
	
	client.nick = client.request.headers.cookie.nick;  
	client.servidor = client.request.headers.cookie.servidor;
	client.canal = client.request.headers.cookie.canal;


	var irc_client = new irc.Client(client.servidor, client.nick);


	irc_client.addListener('registered', function(message){
		socket.emit('registrado', "Voce está registrado no irc");
		Join(client, client.canal);	
	});

	irc_client.addListener('motd', function(motd){
		socket.emit('motd', '<pre>'+motd+'</pre>');			
	});

	irc_client.addListener('error', function(message){
		socket.emit('erro', message.args[2]);
	});

	irc_client.addListener('nick', function(oldnick, newnick, channels){
		socket.emit('nick', {'velhonick': oldnick,
		'novonick':newnick, 
		'canais':channels });
	});



	irc_client.addListener('selfMessage', function(to, text)
	{
		socket.emit('privmsg', to);
	});

	irc_client.addListener('list', function(channels)
	{
		socket.emit('list', channels);
	});

	irc_client.addListener('pingpong', function(pong)
	{
		socket.emit('pingpong', pong);
	});


	irc_client.addListener('join', function(channel)
	{
		socket.emit('join', channel);
	});
	
	irc_client.addListener('quit', function(nick, reason, channels, message){
		socket.broadcast.emit('quit', nick);
		client.disconnect();
	});

	irc_client.addListener('invite', function(channel, from, message) {
		socket.emit('invite', {'canal': channel, 'from': from, 'msg': message});
	});
	
	irc_client.addListener('whois', function(info)
	{
		socket.emit('whois', info);
	});

	irc_client.addListener('message', function(nick, to, text, msg){
		

		var mensagem = '&lt' + nick + '&gt ' + text;

		socket.emit('message',mensagem);
	});

	client.irc_client = irc_client;
	clients[proxy_id] = client;

	socket.on('message', function (msg) {


				
		if(msg.charAt(0) == '/')
		{

			var comando = msg.split(' ');

			switch(comando[0].toUpperCase())
			{
				
				case '/NICK': Nick(comando[1], client);
				break;

				case '/MOTD': client.irc_client.send('motd');
				break;

				case '/PRIVMSG' : Privmsg(comando, client);
				break;

				case '/LIST' : List(client);
				break;

				case '/QUIT': client.irc_client.emit('quit', client.nick, msg, client.canal.toString());
				break;

				case '/INVITE': Invite(comando[1], comando[2], client, clients);
                break;

				case '/PING' : Ping(client);
				break;


				case '/JOIN' : Join(client, comando[1]);
				break;

				case '/WHOIS': Whois(comando[1],client);
				break;
				
				default: client.emit('erro', 'Comando inexistente.');
			}

		}
		else
		{
			PrivmsgChannel(msg, client);
		}
	});
});

app.post('/login', function (req, res) 
{ 
	res.cookie('nick', req.body.nome);

	if(req.body.canal && req.body.canal[0]!='#')
	{
			req.body.canal = '#'+req.body.canal;
	}

	res.cookie('canal', req.body.canal);
	res.cookie('servidor', req.body.servidor);
	res.redirect('/');
});

server.listen(8080, function () {				
  console.log('Servidor rodand no endereço: http://localhost:8080/');	
});
